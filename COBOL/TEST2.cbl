       IDENTIFICATION DIVISION.
       PROGRAM-ID. TEST1.
      *****************************************************************
      *                                                               *
      *   Simple Prog for zUnit                                       *
      *                                                               *
      *****************************************************************

       ENVIRONMENT DIVISION.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       LINKAGE SECTION.
       01 RCV-PARMS.
          05 IN-TEXT    Pic x(20).
          05 OUT-TEXT   Pic x(41).

       PROCEDURE DIVISION USING RCV-PARMS.
       MAIN.
           DISPLAY "DevOps Demo FEB3"

           Initialize OUT-TEXT.

           MOVE IN-TEXT TO OUT-TEXT(1:20).
           MOVE SPACE TO OUT-TEXT(21:1).
           MOVE IN-TEXT TO OUT-TEXT (22:20).

           DISPLAY OUT-TEXT
           DISPLAY "End OF PROGRAM"
           GOBACK.

       END PROGRAM TEST1.