      ******************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.  BUILDCMP.
       AUTHOR.      VINAY.
       DATE-WRITTEN. 5/22/2017.
      ******************************************************
      *PROGRAM TITLE : COMPARING CONTENT OF TWO FILES
      ******************************************************
      *PROGRAM OVERVIEW : THIS PROGRAM READS A PS FILE AND
      *THEN READS A VSAM FILE USING KEY AS MEMBER NAME
      *EXTRACTED FROM PS FILE.IF THE RECORD IS FOUND THEN IT
      *CHECKS THE DATE AND TIMESTAMP.
      *****************************************************
      *FILES USED AND CALLED MODULES
      *****************************************************
      *NAME        USE        DESCRIPTION
      *----       -----      -------------
      *
      *
      *****************************************************
      *MODIFICATION LOG
      *****************************************************
      *REQ NO   DATE   MOD BY      DESCRIPTION
      *------   ----   ------      -----------
      *
      *****************************************************
       ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
            SELECT INFILE1 ASSIGN TO INPUT1
            FILE STATUS IS WS-INFILE1-STAT.

            SELECT INFILE2
                    ASSIGN TO INPUT2
                    ORGANIZATION IS INDEXED
                    ACCESS MODE  IS RANDOM
                    RECORD KEY IS IN2-MEM-NAME
                    FILE STATUS IS WS-INFILE2-STAT.

            SELECT OUTFILE ASSIGN TO OUTPUT1
            FILE STATUS IS WS-OUTFILE-STAT.

       DATA DIVISION.
       FILE SECTION.

       FD INFILE1
            BLOCK CONTAINS 0 RECORDS
            LABEL RECORDS ARE STANDARD.
       01 IN1-RECORD.
          05 IN1-MEM-NAME                PIC X(8).
          05 FILLER1                     PIC X(1).
          05 IN1-DATE.
             10 IN1-YEAR                 PIC 9(4).
             10 FILLER2                  PIC X(1).
             10 IN1-MONTH                PIC 9(2).
             10 FILLER3                  PIC X(1).
             10 IN1-DAY                  PIC 9(2).

          05 IN1-TIMESTAMP.
             10 FILLER4                  pic 9(1).
             10 IN1-HOURS                PIC 9(2).
             10 FILLER5                  PIC X(1).
             10 IN1-MINUTES              PIC 9(2).
             10 FILLER6                  PIC X(1).
             10 IN1-SECONDS              PIC 9(2).
          05 FILLER5                     PIC X(52) VALUE SPACES.

       FD INFILE2
            BLOCK CONTAINS 0 RECORDS
            LABEL RECORDS ARE STANDARD.
       01 IN2-RECORD.
          05 IN2-MEM-NAME               PIC X(8).
          05 IN2-DATE2.
             10 IN2-MONTH               PIC 9(2).
             10 FILLER7                 PIC X(1).
             10 IN2-DAY                 PIC 9(2).
             10 FILLER8                 PIC X(1).
             10 IN2-YEAR                PIC 9(4).
          05 IN2-TIMESTAMP.
             10 FILLER9                 PIC X(2).
             10 IN2-HOURS               PIC 9(2).
             10 FILLER10                PIC X(1).
             10 IN2-MINUTES             PIC 9(2).
             10 FILLER11                PIC X(1).
             10 IN2-SECONDS             PIC 9(2).
          05 FILLER12                   PIC X(52) VALUE SPACES.

       FD OUTFILE
            BLOCK CONTAINS 0 RECORDS
            LABEL RECORDS ARE STANDARD.
       01 OUT-RECORD.
          05 OUT-MEM-NAME               PIC X(8).
          05 FILLER10                   PIC X(72) VALUE SPACES.



       WORKING-STORAGE SECTION.

       01 WS-INFILE1-EOF                 PIC X VALUE SPACES.
       01 WS-INFILE2-EOF                 PIC X VALUE SPACES.
       01 WS-OUTFILE-EOF                 PIC X VALUE SPACES.

       01 IN-FILE-STAT                   PIC 9(2) VALUE ZEROS.
          88 IN-SUCC                              VALUE ZEROS.

       01 WS-INFILE1-STAT               PIC 9(2) VALUE ZEROS.
       01 WS-INFILE2-STAT               PIC 9(2) VALUE ZEROS.
       01 WS-OUTFILE-STAT               PIC 9(2) VALUE ZEROS.
       01 WS-MEM-CHK                    PIC X(2) VALUE SPACES.
          88 WS-MEM-CHK-YES                       VALUE 'Y'.
          88 WS-MEM-CHK-NO                        VALUE 'N'.

       01 WS-MEM-TEMP                   PIC X(11) VALUE SPACES.
       01 WS-MEM-NAME                   PIC X(8)  VALUE SPACES.
       01 WS-5695PMB0                   PIC X(9)  VALUE SPACES.

       01 WS-OUT-REC.
          05 WS-OUT-MEM-NAME            PIC X(8).
          05 WS-OUT-TIME-STAMP          PIC X(31).
          05 FILLER                     PIC X(41) VALUE SPACES.

       01 IN1-TEMP-DATE.
             10 IN1-TEMP-YEAR               PIC 9(4).
             10 IN1-TEMP-MONTH              PIC 9(2).
             10 IN1-TEMP-DAY                PIC 9(2).
       01 IN2-TEMP-DATE.
             10 IN2-TEMP-YEAR               PIC 9(4).
             10 IN2-TEMP-MONTH              PIC 9(2).
             10 IN2-TEMP-DAY                PIC 9(2).

       01 IN1-TEMP-TIMESTAMP.
             10 IN1-TEMP-HOURS               PIC 9(2).
             10 IN1-TEMP-MINUTES             PIC 9(2).
             10 IN1-TEMP-SECONDS             PIC 9(2).
       01 IN2-TEMP-TIMESTAMP.
             10 IN2-TEMP-HOURS               PIC 9(2).
             10 IN2-TEMP-MINUTES             PIC 9(2).
             10 IN2-TEMP-SECONDS             PIC 9(2).

       PROCEDURE DIVISION.
      *****************************************************
      *MAIN PROGRAM DRIVER.
      *****************************************************
       0000-XXXX.

           PERFORM 1000-PROGRAM-INITIALIZATION.
           PERFORM 2000-MAIN-PROCESSING.
           PERFORM 3000-PROGRAM-WRAPUP.

      *****************************************************
      *PROGRAM INITIALIZATION PARAGRAPH
      *****************************************************
       1000-PROGRAM-INITIALIZATION.

           OPEN INPUT INFILE1

           IF WS-INFILE1-STAT = '00'
              CONTINUE
           ELSE
              DISPLAY "INFILE1 OPEN ISSUE" WS-INFILE1-STAT
              GO TO 3000-PROGRAM-WRAPUP
           END-IF

           OPEN INPUT INFILE2

           IF WS-INFILE2-STAT = '00'
              CONTINUE
           ELSE
              DISPLAY "INFILE2 OPEN ISSUE" WS-INFILE2-STAT
              GO TO 3000-PROGRAM-WRAPUP
           END-IF

           OPEN OUTPUT OUTFILE

           IF WS-OUTFILE-STAT = '00'
              CONTINUE
           ELSE
              DISPLAY "OUTPUT OPEN ISSUE" WS-OUTFILE-STAT
              GO TO 3000-PROGRAM-WRAPUP
           END-IF
           .
       1000-EXIT.
           EXIT.

      ****************************************************
      *MAIN PROCESSING PARAGRAPH
      ****************************************************
       2000-MAIN-PROCESSING.

            PERFORM 2100-READ-INPUT1-FILE
                    UNTIL WS-INFILE1-EOF = 'Y'.
           
       2000-EXIT.
            EXIT.


      ***************************************************
      *FILE READ
      ***************************************************
       2100-READ-INPUT1-FILE.

           READ INFILE1
                AT END MOVE 'Y' TO WS-INFILE1-EOF
           END-READ.

           IF WS-INFILE1-EOF = 'Y'
              GO TO 3000-PROGRAM-WRAPUP
           END-IF.

           IF WS-INFILE1-STAT = '00' OR '04'
              CONTINUE
           ELSE
             DISPLAY "IN FILE READ ISSUE" IN-FILE-STAT
           END-IF

           MOVE   IN1-MEM-NAME TO IN2-MEM-NAME
      **READ INPUT2 FILE WITH THE KEY FROM INPUT1 FILE
           READ INFILE2
                KEY IS IN2-MEM-NAME
                INVALID KEY DISPLAY 'INVALID KEY'
           END-READ

           IF WS-INFILE2-STAT = 10 OR 23
      **IF EOF OR NO RECORD FOUND THEN ITS A NEW MEMBER;SO WRITE
              PERFORM 2200-WRITE-LOGIC
           ELSE IF WS-INFILE2-STAT = 00
      **RECORD IS FOUND
                   PERFORM 2150-COMPARE-LOGIC
                ELSE
                   DISPLAY " ISSUE WITH VSAMFILE" WS-INFILE2-STAT
                   GO TO  3000-PROGRAM-WRAPUP
               END-IF
           END-IF
           .
       2100-EXIT.
            EXIT.

      ***************************************************
      *DATE AND TIMESTAMP COMPARE LOGIC
      ***************************************************
       2150-COMPARE-LOGIC.

            IF IN1-MEM-NAME = IN2-MEM-NAME
      *DATE MOVEMENT LOGIC
               MOVE IN1-YEAR  TO IN1-TEMP-YEAR
               MOVE IN1-MONTH TO IN1-TEMP-MONTH
               MOVE IN1-DAY   TO IN1-TEMP-DAY

               MOVE IN2-YEAR  TO IN2-TEMP-YEAR
               MOVE IN2-MONTH TO IN2-TEMP-MONTH
               MOVE IN2-DAY   TO IN2-TEMP-DAY
               DISPLAY IN1-MEM-NAME':' IN1-TEMP-DATE
               DISPLAY IN1-MEM-NAME ':'IN1-TIMESTAMP
               DISPLAY IN2-MEM-NAME ':'IN2-TIMESTAMP
               DISPLAY IN2-MEM-NAME':'IN2-TEMP-DATE

               IF IN1-TEMP-DATE > IN2-TEMP-DATE
                  PERFORM 2200-WRITE-LOGIC
               ELSE IF IN1-TEMP-DATE = IN2-TEMP-DATE
      *TIMESTAMP MOVEMENT LOGIC
                        DISPLAY IN1-HOURS IN1-MINUTES
                        MOVE IN1-HOURS       TO IN1-TEMP-HOURS
                        MOVE IN1-MINUTES     TO IN1-TEMP-MINUTES
                        MOVE IN1-SECONDS     TO IN1-TEMP-SECONDS
                        DISPLAY IN2-HOURS IN2-MINUTES

                        MOVE IN2-HOURS       TO IN2-TEMP-HOURS
                        MOVE IN2-MINUTES     TO IN2-TEMP-MINUTES
                        MOVE IN2-SECONDS     TO IN2-TEMP-SECONDS

                        DISPLAY IN1-TEMP-TIMESTAMP':'IN2-TEMP-TIMESTAMP
                        IF IN1-TEMP-TIMESTAMP > IN2-TEMP-TIMESTAMP
                           DISPLAY IN1-MEM-NAME
                           PERFORM 2200-WRITE-LOGIC
                        END-IF
                    END-IF
               END-IF

            ELSE
                DISPLAY " ISSUE WITH READ VSAM"
                GO TO  3000-PROGRAM-WRAPUP
            END-IF
            .
       2150-EXIT.
                                                      EXIT.

      ***************************************************
      *OUTPUT WRITE LOGIC
      ***************************************************
       2200-WRITE-LOGIC.
      *WRITE LOGIC**
           MOVE  IN1-MEM-NAME TO OUT-MEM-NAME
            WRITE OUT-RECORD
            .
       2200-EXIT.
            EXIT.

      ****************************************************
      *PROGRAM WRAPUP
      ****************************************************
       3000-PROGRAM-WRAPUP.

            CLOSE INFILE1,INFILE2,OUTFILE.
            STOP RUN.

       3000-EXIT.
            EXIT.