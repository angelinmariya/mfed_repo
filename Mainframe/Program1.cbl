000100 IDENTIFICATION DIVISION.
000200 PROGRAM-ID. Program1.
000300*****************************************************************
000400*                                                               *
000500*   Simple Prog for zUnit                                       *
000600******                                                            
      * *
000700*****************************************************************
000800/
000900 ENVIRONMENT DIVISION.
001000 DATA DIVISION.
001100 WORKING-STORAGE SECTION.
001200 01 WS-A         PIC X(20).
001300 01 WS-B         PIC X(20).
001400 LINKAGE SECTION.
001500 01 RCV-PARMS.
001600 05 IN-TEXT    Pic x(20).
001700    05 OUT-TEXT   Pic x(41).
001800 PROCEDURE DIVISION USING RCV-PARMS.
001900 MAIN.
002000     DISPLAY "Start 1"
002100     Initialize OUT-TEXT.
002200     MOVE IN-TEXT TO OUT-TEXT(1:20).
002300     MOVE SPACE TO OUT-TEXT(21:1).
002400     MOVE IN-TEXT TO OUT-TEXT (22:20).
002500     MOVE IN-TEXT TO WS-A.
002600     MOVE WS-A  TO WS-B.
002700     
002800     DISPLAY OUT-TEXT
002900     DISPLAY "End"
003000     GOBACK.
003100 END PROGRAM Program1.
003200
003300